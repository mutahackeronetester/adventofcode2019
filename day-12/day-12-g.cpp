#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <future>

#include "Arithmetic.h"

#define N_STEP 50'000'000'000

using namespace std;



struct Moon {
  vector<int> pos, vel;

  Moon(){};
  Moon(int x, int y, int z):pos({x,y,z}),vel(3,0){}


  void applyGrav(const Moon& other){
    auto v = vel.begin();
    transform( pos.begin(), pos.end(), other.pos.begin(), vel.begin(), [&v] (int p1, int p2){
        return *(v++) + p1-p2 > 0 ? -1 : p1!=p2; 
        });
  }

  void applyVel(){
    transform( pos.begin(), pos.end(), vel.begin(), pos.begin(), plus<int>());
  }

  int energy(){
    int pot=0;
    for_each( pos.begin(), pos.end(), [&pot] (int p){ 
        pot+=abs(p);
        });

    int kin=0;
    for_each( vel.begin(), vel.end(), [&kin] (int v){
        kin+=abs(v);
        });
    return pot*kin;
  }
};

struct D1Moon{
  int pos, vel;

  D1Moon(){};
  D1Moon(int p):pos(p),vel(0){};

  void applyGrav( const D1Moon& other ){
    vel+= ( other.pos < pos ? -1 : pos!=other.pos);
  }
  void applyVel(){ pos += vel; };
};

bool operator==( const D1Moon& m1, const D1Moon& m2 ){
  return m1.pos == m2.pos && m1.vel == m2.vel;
}


ostream& operator<<(ostream& out, const D1Moon& m ){
  out << "pos:" << m.pos << " vel:" << m.vel;
  return out;
}

template<typename A>
struct Sys {
  vector<A> vma, vma0;
  int res;

  Sys():res(0){};

  void push( A a ){
    vma.push_back(a);
    vma0.push_back(a);
  }
 
  void step(){

    res ++;
    if ( ! (res % 100'000) ){ cerr << "step n:" << res << endl; }
    for ( A& m1 : vma ){
      for ( A& m2 : vma ){
        m1.applyGrav(m2);
      }
    }
    for ( A& m : vma){ m.applyVel(); }

    //copy( vma.begin(), vma.end(), ostream_iterator<D1Moon>(cerr, " "));
    //cerr << endl;
  }

  int compute(){
    do {
      step();
    }
    while( !equal( vma.begin(), vma.end(), vma0.begin() ));
    return res;
  }
};




ostream& operator<<(ostream& out, const Moon& m){
  out << "<x:" << m.pos[0] << " y:" << m.pos[1] << " z:" << m.pos[2] << ">";
  out << " <vx:" << m.vel[0] << " vy:" << m.vel[1] << " vz:" << m.vel[2] << ">";
  out << endl;
  return out;
}

bool operator==( const Moon& m1, const Moon& m2 ){
  return equal( m1.vel.begin(), m1.vel.end(), m2.vel.begin()) && 
    equal( m1.pos.begin(), m1.pos.end(), m2.pos.begin());
}

int main(){

  vector<Sys<D1Moon>> sys(3);

  for( int j = 0; j < 4; j++) { 

      string h[3];
      cin >> h[0] >> h[1] >> h[2];
      h[0] = h[0].substr( 3, h[0].size()-4 );
      h[1] = h[1].substr( 2, h[1].size()-3 );
      h[2] = h[2].substr( 2, h[2].size()-3 );

      int i = 0;
      for ( string hh : h ){
        sys[i++].push( D1Moon( atoi( hh.c_str())));
      }
  }



  future<int> future0 =  async( [&sys] { return sys[0].compute(); } );
  future<int> future1 =  async( [&sys] { return sys[1].compute(); } );
  future<int> future2 =  async( [&sys] { return sys[2].compute(); } );

  cout << ppcm( future0.get(), ppcm( future1.get(), future2.get()));

  /*
  auto asi = as.begin();
    *(asi++) = async( [&s] { return s.compute(); } );
  }
  */

  /*
  transform( as.begin(), as.end(), sys.begin(), as.begin(), [] (future<int> fut, Sys<D1Moon> s) {
      return ;
      });
      */
  //cout << as[0].get();

  //cout << ppcm( as[0].get(), ppcm( as[1].get(), as[2].get() )) << endl; 
}
