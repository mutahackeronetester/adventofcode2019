#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <iterator>

#define N_STEP 50'000'000'000

using namespace std;



struct Moon {
  vector<int> pos, vel;

  Moon(){};
  Moon(int x, int y, int z):pos({x,y,z}),vel(3,0){}


  void applyGrav(const Moon& other){
    auto v = vel.begin();
    transform( pos.begin(), pos.end(), other.pos.begin(), vel.begin(), [&v] (int p1, int p2){
        return *(v++) + p1-p2 > 0 ? -1 : p1!=p2; 
        });
  }

  void applyVel(){
    transform( pos.begin(), pos.end(), vel.begin(), pos.begin(), plus<int>());
  }

  int energy(){
    int pot=0;
    for_each( pos.begin(), pos.end(), [&pot] (int p){ 
        pot+=abs(p);
        });

    int kin=0;
    for_each( vel.begin(), vel.end(), [&kin] (int v){
        kin+=abs(v);
        });
    return pot*kin;
  }

};

ostream& operator<<(ostream& out, const Moon& m){
  out << "<x:" << m.pos[0] << " y:" << m.pos[1] << " z:" << m.pos[2] << ">";
  out << " <vx:" << m.vel[0] << " vy:" << m.vel[1] << " vz:" << m.vel[2] << ">";
  out << endl;
  return out;
}

bool operator==( const Moon& m1, const Moon& m2 ){
  return equal( m1.vel.begin(), m1.vel.end(), m2.vel.begin()) && 
    equal( m1.pos.begin(), m1.pos.end(), m2.pos.begin());
}

int main(){

  vector<Moon> vm(4), vm0;
  vector<int> acc(3,0);
  vector<bool> f(3,true);
  int res=0;

  generate(vm.begin(), vm.end(), [] {
      string x,y,z;
      cin >> x >> y >> z;
      x = x.substr( 3, x.size()-4 );
      y = y.substr( 2, y.size()-3 );
      z = z.substr( 2, z.size()-3 );
      return Moon( atoi(x.c_str()), atoi(y.c_str()), atoi(z.c_str()) );
      });

  vm0 = vm;
  //copy( vm.begin(), vm.end(), ostream_iterator<Moon>(cerr));
  //for ( int i=N_STEP; i>0; i--){

  do {
    //cerr << "step i:" << N_STEP - i << endl;
    transform( f.begin(), f.end(), acc.begin(), acc.begin(), [] ( bool ff, int a ){
        return a+ff;})
    res++;

    if ( !(res % 100'000) ){
      cerr << "step i:" << res << endl;
    }

    for ( Moon& m1 : vm ){
      for ( Moon& m2 : vm ){
        m1.applyGrav(m2);
      }
    }
    
    for ( Moon& m : vm){ m.applyVel(); }

    transform( f.begin(), f.end(), f.begin(), [&ff] (Moon p1, Moon p2) { 
        vm.begin(), vm.end(), vm0.begin(),
        });
    //copy( vm.begin(), vm.end(), ostream_iterator<Moon>(cerr) );
  }
  while ( !equal(vm.begin(), vm.end(), vm0.begin()) && res < N_STEP );

  //for_each( vm.begin(), vm.end(), [&res] (Moon m) { res += m.energy();});
  cout << res << endl; 
}
