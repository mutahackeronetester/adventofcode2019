#include <line>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>

#define DECK_SIZE 10006
#define GOLD_CARD 2019

using namespace std;


class Deck {

  vector<int> deck;

  public:

  Deck(int N)deck(N){
    int i = 0;
    generate(deck.begin(), deck.end(), [&i] { return i++;})
  }

  int search( int card ){
    return find(deck.begin(), deck.end(), card) - begin();
  }

  void newstack(){
    reverse(deck.begin(), deck.end());
  }

  void cut(int n){
    auto middle = n > 0 ? deck.end()-n-1 : deck.begin()-n;
    rotate(deck.begin(), middle, deck.end())
  }

  void incremental(int n){
    int N = deck.size();
    vector<int> copy(N);
    for(int i =0; i<N; i++){
      copy[ i*n % N] = deck[i];
    }
    deck = copy;
  }

}

int main(){


  string line, sub;

  Deck deck(DECK_SIZE);

  while( getline( cin, line)){
    if( line[0] == 'c' ){
      sub = substr(4, line.size()-4);
      cerr << "cut n:" << sub << endl;
      deck.cut( atoi( sub.c_str() ));
    } else if ( line[6] == 'i'){
      cerr << "reverse" << endl;
      deck.reverse();
    } else { 
      string sub = substr(21, line.size() - 21);
      cerr << "incremental n:" << sub << endl;
      deck.incremental( atoi( sub.c_str()));
    }
  };


  cout << deck.search( GOLD_CARD ) << endl;


}
