#include <iostream>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iterator>


#ifndef INTCODE
#define INTCODE


/**
 * Ad-oc compiler specified and tested step by step by problems : 
 * day-2 
 * day-5 
 * day-7 
 * day-9 
 *
 * Used to obfuscate problems result. Like day-13 and day-15, that would 
 * otherwise be hackable by looking at problem input. 
 *
 */
/* Template because big integers must be supported for day-11 */ 
template<typename T>
class IntCode_4 : public IntCode_3 {


  /* "heap" hold the program computed, 
   * loaded from shell input or from file.
   * Cannot be const, because it is used to store variables as well.
   * */
  std::vector<T> heap;

  /* base used for relative adresses
   */
  int r_base = 0;


  /* Variadic template */
  template<typename... A>
  /* read several operations arguments, according to specification
   * given in day-5, in case of setting a value.
   * */
  void args_set( std::vector<T>::iterator i, int& pos, A&... a);


  /* Variadic template */
  template<typename... A>
  /* read several operations arguments, according to specification
   * given in day-5, in case of binary/unary operation.
   * */
  int args( std::vector<T>::iterator i, A&... a);

  /* unfolded read of argument, 
   * with absolute or relative address or with value.
   * */
  void one_arg( std::vector<T>::iterator i, int& b, long long* a);


  /* generic operation code.
   * returns default error.
   * every operation is a specialization of this template. 
   * */
  template<int opcode>
  void operation( std::vector<T>::iterator i );

public:

  /* Shared IO channels.
   */
  std::shared_ptr<Channel> in, out;

  /* Debugging facility. 1 == DEBUG, 2 == TRACE
   * */
  int verbose = 0;


  IntCode();
  IntCode( std::vector<T>& h, std::shared_ptr<Channel> i, std::shared_ptr<Channel> o );
  /* non standard copy */
  ~IntCode();

  /* set full heap */
  void load( std::vector<T> a );
  /* update one value */
  void load( int pos, T v );

  /* debbug api compute one operation */
  int next_operation( std::vector<T>::iterator i);

  /* debbug api finished at first output */
  int next_result();

  /* end at termination operation (code 99) */
  int compute(); 
};


std::istream & operator>> ( std::istream & in,  IntCode& cp );



/**
 * Abstract interface of channels.
 * Might be extended for specific behavior.  
 */
template<typename NUM>
class AbstractChannel {

  public: 

  virtual NUM get() = delete;
  virtual void put( NUM val ) = delete;
};




/** 
 * Basic Channel implementation
 * */
template<typename NUM>
class Channel{
  NUM val; 
  bool set;
  std::mutex c;
  std::condition_variable cv;

public:

  Channel();
  Channel( NUM val );

  void put( NUM val);

  NUM get();

  NUM get( long ms, bool& isto);

  bool isSet();
};



#endif

