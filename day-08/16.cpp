#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>


#define HFRAME 6
#define WFRAME 25
#define SFRAME ( HFRAME * WFRAME)

using namespace std;

struct Frame {

  char rows[SFRAME];

  Frame(){};
  Frame(const Frame& other){
    copy( other.rows, other.rows + SFRAME, this->rows);
  }
};

istream& operator>> ( istream& in, Frame& f ){
  char c;
  in.get( f.rows, SFRAME+1);
  return in;
}


void print_frame( Frame& s ){
  cout << endl;
  for( int j = 0; j < SFRAME; j+= WFRAME ){
    replace_copy( s.rows + j, s.rows + WFRAME + 1 + j, ostream_iterator<char>(cout), '0', '.' );
    cout << endl;
  }
}


int main(){

  Frame f,s;
  vector<Frame> vf;

  while( cin >> f ){
    vf.push_back(f);
  }


  int i=-1;
  generate( s.rows, s.rows + SFRAME + 1, [&vf, &i] () {
      i++;
      auto ff = find_if(vf.begin(), vf.end(), [&i] ( Frame& f) {
          return f.rows[i] != '2';
          });
      if ( ff == vf.end() ){
        cerr << "px transparent" << endl;
      }
      return ff->rows[i];
      });

  cerr << " i:" << i;

  cerr << endl;
  copy( s.rows, s.rows + SFRAME + 1, ostream_iterator<char>(cerr, " ") );

  print_frame(s);
}
