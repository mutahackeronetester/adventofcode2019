#include <iostream>
#include <cstdio>
#include <iterator>
#include <algorithm>
#include <vector>


using namespace std;

int main(){

  int input, r, a, b, c, d;
  vector<int> prog;


  while ( cin >> a ){ cin.ignore(), prog.push_back(a); }

  auto i = prog.begin();


  r=0;
  while ( !r && i != prog.end() ){



    b = *i / 100;

    switch ( *i % 100 ) { 
      case 1:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        b/=10;
        i++;
        c = ( b % 10 ? *i : prog[*i] );
        d = *(++i);
        cerr << a << " + " << c << " to pos " << d << endl;   
        prog[d] = a + c;
        break;
      case 2:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        b/=10;
        i++;
        c = ( b % 10 ? *i : prog[*i] );
        d = *(++i);
        cerr << a << " * " << c << " to pos " << d << endl;   
        prog[d] = a * c;
        break;
      case 3:
        cin >> a;
        d = *(++i);
        cerr << " input given:" << a << " to pos:" << d << endl;
        prog[d] = a;
        break;
      case 4:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        cout << " output from prog : " << a << endl;
        break;
      case 5:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        i++;
        b/=10;
        c = ( b % 10 ? *i : prog[*i] );
        if ( a ){
          i = prog.begin() + (c-1); 
        }
        cerr << "jump to: " << c << " if: " << a << endl;  
        break;
      case 6:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        i++;
        b/=10;
        c = ( b % 10 ? *i : prog[*i] );
        if ( !a ){
          i = prog.begin() + (c-1); 
        }
        cerr << "jump to: " << c << " if not: " << a << endl;  
        break;
      case 7:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        i++;
        b/=10;
        c = ( b % 10 ? *i : prog[*i] );
        d = *( ++i);
        prog[d] = a < c;
        cerr << "test if " << a << " < " << c << " result in:" << d << endl;  
        break;
      case 8:
        i++;
        a = ( b % 10 ? *i : prog[*i] );
        i++;
        b/=10;
        c = ( b % 10 ? *i : prog[*i] );
        d = *( ++i );
        prog[d] = a == c;
        cerr << "test if " << a << " == " << c << " result in:" << d << endl;  
        break;
      case 99:
        r=prog[0];
        cout << " termination of prog : " << r; 
        break;
      default:
        cerr << "wrong op code : " << *i << endl;
        exit(1);
    } 

    i++;
  }



}
