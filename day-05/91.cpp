#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>


using namespace std;

int main(){

  int input, r, a, b;
  vector<int> prog;


  while ( cin >> a ){ cin.ignore(), prog.push_back(a); }

  auto i = prog.begin();
  r=0;
  while ( !r && i != prog.end() ){

    b = *i / 100;

    switch ( *i % 100 ) { 
      case 1:
        a = ( b % 10 ? *( ++i) : prog[*i] );
        b/=10;
        a += ( b % 10 ? *( ++i) : prog[*i] );
        prog[*(++i)] = a;
        break;
      case 2:
        a = ( b % 10 ? *( ++i) : prog[*i] );
        b/=10;
        a *= ( b % 10 ? *( ++i) : prog[*i] );
        prog[*(++i)] = a;
        break;
      case 3:
        cin >> a;
        prog[*(++i)] = a;
        break;
      case 4:
        a = ( b % 10 ? *( ++i) : prog[*i] );
        cout << " output from prog : " << a << endl;
        break;
      case 99:
        r=prog[0];
        cout << " termination of prog : " << r; 
        break;
      default:
        cerr << "wrong op code : " << *i << endl;
        exit(1);
    } 
    i++;
  }

    if( !(n % 10)  && !( v%10) ){
      //copy(prog.begin(), prog.end(), ostream_iterator<int>(cerr, ","));
      cerr << "n:" << n << " v:" << v << endl;
      cerr << "r:" << r << endl;
      cerr << endl;
    }
  }

  if ( r == e ){
    cout << "found n:" << n << " and v:" << v << endl;
  } else {
    cerr << "result expected not found." << endl;
  }

  

}
