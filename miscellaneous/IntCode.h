#include <iostream>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>

#ifndef INTCODE
#define INTCODE



template<typename NUM>
class AbstractChannel {

  public: 

  virtual NUM get() = delete;
  virtual void put( NUM val ) = delete;
};




class Channel{
  long long val; 
  bool set;
  std::mutex c;
  std::condition_variable cv;

public:

  Channel();
  Channel( long long val );

  void put( long long val);

  long long get();

  // set isto to true is get has timeout.  
  long long get( long ms, bool& isto);

  bool isSet();
};


class IntCode{

  template<typename... A>
  void args_set( std::vector<long long>::iterator& i, int& pos, A&... a);
  template<typename... A>
  int args( std::vector<long long>::iterator& i, A&... a);
  void one_arg( std::vector<long long>::iterator& i, int& b, long long* a);

  template<int opcode>
  void operation( std::vector<long long>::iterator& i );


public:
  std::vector<long long> heap;
  std::shared_ptr<Channel> in, out;
  int verbose = 0;
  int r_base = 0;

  IntCode();
  IntCode( std::vector<long long>& h, std::shared_ptr<Channel> i, std::shared_ptr<Channel> o );
  ~IntCode();

  void load( std::vector<long long> a );
  int next_operation( std::vector<long long>::iterator& i);

  int next_result();
  int compute(); 
};


std::istream & operator>> ( std::istream & in,  IntCode& cp );



#endif

