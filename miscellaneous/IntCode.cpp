#include <iostream>
#include <cstdio>
#include <iterator>
#include <algorithm>
#include <vector>
#include <memory>
#include <condition_variable>
#include <chrono>
#include <exception>
#include "IntCode.h"


using namespace std;



Channel::Channel():val(0),set(false){};


Channel::Channel(long long v):val(v),set(true){};


void Channel::put(long long v){

  unique_lock<mutex> lck(c);
  cv.wait(lck, [&] { return !set; });
  val=v;
  set=true;
  cv.notify_one();
}


long long Channel::get( long mstimeout, bool& isto){
  
  unique_lock<mutex> lck(c);
  isto = !cv.wait_for(lck, chrono::milliseconds(mstimeout) , [&] { return set; });
  set=false;
  cv.notify_one();
  return val;
}

long long Channel::get(){

  unique_lock<mutex> lck(c);
  cv.wait(lck, [&] { return set; });
  set=false;
  cv.notify_one();
  return val;
}


bool Channel::isSet(){
  return true;
}



IntCode::IntCode(){
  in = make_shared<Channel>();
  out = make_shared<Channel>();
};
IntCode::IntCode( vector<long long>& h, shared_ptr<Channel> i, shared_ptr<Channel> o ):in(i),out(o){
  load(h);
}

IntCode::~IntCode(){}


void IntCode::load( vector<long long> a ){
  heap = a;
}



// ref: https://stackoverflow.com/questions/12515616/expression-contains-unexpanded-parameter-packs/12515637#12515637
template<typename... A>
int IntCode::args( vector<long long>::iterator& i, A&... a ){

  int modes = *i/100;
  i++; // go to first arg
  int dummy[] {(one_arg(i, modes, &a), 0)...};
  return modes;
}

template<typename... A>
void IntCode::args_set( vector<long long>::iterator& i, int& b, A&... a ){
  /* When a result has to be set, last position is returned,
   * instead of the current value of the slot
   */
  int m = args(i, a...);
  b = ( m == 2 ) ? r_base + *i : *i;

  i++;
}

void IntCode::one_arg( vector<long long>::iterator& i, int& modes, long long* a){
  /* Determine argument with first digit of modes:
   *  if 0: address mode
   *  if 1: direct mode
   *  if 2: relative mode
   */
  *a = ( modes % 10 ? (modes-1) % 10 ? heap[ *i + r_base ] : *i : heap[*i] );
  modes/=10;
  i++;
}


template<int opcode>
void IntCode::operation( vector<long long>::iterator& i ){
  throw "wrong op code:" + *i%100;
}


template<> 
void IntCode::operation<1>( vector<long long>::iterator& i ){
  long long a,b;
  int c;
  args_set(i, c, a, b);
  heap[c]=a+b;

  if ( verbose == 2 ){
    cerr << a << " + " << b << " to:" << c << endl;   
  }
}


template<>
void IntCode::operation<2>( vector<long long>::iterator& i ){
  long long a,b;
  int c;
  args_set(i, c, a, b);
  heap[c]=a*b;

  if ( verbose == 2 ){
    cerr << a << " * " << b << " to:" << c << endl;   
  }
}


template<>
void IntCode::operation<3>( vector<long long>::iterator& i ){

  if ( verbose ){
      cerr << " get:" ;
  }

  int a;
  args_set(i, a);
  heap[a] = in->get();

  if (verbose ){
    cerr << heap[a] << " to:" << a << endl;
  }
}


template<>
void IntCode::operation<4>( vector<long long>::iterator& i ){

  long long a; 
  args(i, a);
  out->put(a);
  if ( verbose == 1){
    cerr << " show:" ;
    cerr << a << endl;
  }


}


template<>
void IntCode::operation<5>( vector<long long>::iterator& i ){
  long long a, b; 
  args(i, a, b);

  if ( a ){
    i = heap.begin() + b; 
  }

  if ( verbose == 2){
    cerr << "jumpTo:" << b << " if:" << a << endl;  
  }
}

template<>
void IntCode::operation<6>( vector<long long>::iterator& i ){
  long long a, b;
  args(i, a, b);

  if ( !a ){
    i = heap.begin() + b; 
  }

  if ( verbose == 2){
    cerr << "jumpTo:" << b << " ifNot:" << a << endl;  
  }
}

template<>
void IntCode::operation<7>(vector<long long>::iterator& i){
  long long a,b;
  int c;
  args_set(i, c, a, b);
  heap[c] = a < b;

  if ( verbose == 2){
    cerr << a << " < " << b << " result:" << heap[c] << " in:" << c << endl;  
  }
}


template<>
void IntCode::operation<8>(vector<long long>::iterator& i){
  long long a,b;
  int c;
  args_set(i, c, a, b);
  heap[c] = a == b;

  if ( verbose ==2){
    cerr << a << " == " << b << " result:" << heap[c] << " in:" << c << endl;  
  }
}
 
template<>
void IntCode::operation<9>(vector<long long>::iterator& i){
  long long a; 
  args(i, a);
  r_base+=a;

  if ( verbose ==2 ){
    cerr << "update r_base with:" << a << " res:" << r_base << endl;  
  }
}
 

template<>
void IntCode::operation<99>(vector<long long>::iterator& i){
  if ( verbose ){
    cerr << " stopping" << endl;  
  }
}




int IntCode::next_operation( vector<long long>::iterator& i){

    switch (*i%100) { 
      case 1:
        operation<1>(i);
        break;
      case 2:
        operation<2>(i);
        break;
      case 3:
        operation<3>(i);
        break;
      case 4:
        operation<4>(i);
        break;
      case 5:
        operation<5>(i);
        break;
      case 6:
        operation<6>(i);
        break;
      case 7:
        operation<7>(i);
        break;
      case 8:
        operation<8>(i);
        break;
      case 9:
        operation<9>(i);
        break;
      case 99:
        operation<99>(i);
        break;
      default:
        operation<0>(i);
    }

    return !( (*i+1)%100 ) ;
}



int IntCode::compute( ) {
  
  auto i = heap.begin();

  while (  ! next_operation(i) ){};

  return 1;
}


int IntCode::next_result(){

  auto i = heap.begin();
  int op;

  do { op = *i%100; }
  while (!next_operation(i) && op != 4 );

  return 1;
}


istream & operator>> ( istream & in, IntCode& cp){
  long long k;
  vector<long long> v;
  while( in >> k, in.ignore() ){
    v.push_back(k);
  }
  cp.load(v);
  return in;
}



#ifndef MAIN
#define MAIN

#include <fstream>
#include <memory>
#include <thread>


string AoC_dir="/home/me/workspace/C++/AdventOfCode/";

struct CpTest {
  shared_ptr<ifstream>  f;
  string fname;
  vector<long long> i;
  long long r;

  CpTest(){};

  CpTest(string fname, vector<long long> i, long long r):fname((AoC_dir + fname)),i(i),r(r){
    f = make_shared<ifstream>((AoC_dir + fname).c_str());
    /*
    cerr << "create ";
    if ( f->good() ){
      cerr << "stream is correct" << endl;
    } else {
      cerr << "stream is not good" << endl;
    }
    */
  }

  CpTest(const CpTest& other){
    r = other.r;
    i = other.i;
    fname = other.fname;
    f = make_shared<ifstream>(other.fname.c_str());
  }


};


void compute_thread( IntCode *cp ){
  cerr << "starting thread" << endl;
  cp->compute();
  cerr << "thread finished" << endl;
}


bool launch_test( IntCode& cp, CpTest& i ){
   *(i.f) >> cp;

    cp.verbose = true;

    thread t = thread( compute_thread, &cp );

    for_each( i.i.begin(), i.i.end(), [&cp] (long long a) {
        cp.in->put(a);
        cerr << "input delivered:" << a << endl;
        }); 

    t.join();

    return i.r != cp.out->get();
}


void main1(){

  IntCode cp;
  vector<CpTest> exemples({
      //CpTest("9.input", vector<long long>({1}), 9025675),
      /*CpTest("13.ex1", vector<long long>({0, 4}), 40),
      CpTest("13.ex2", vector<long long>({0, 4}), 45),
      CpTest("13.ex3", vector<long long>({0, 4}), 45),
      *///CpTest("13.input", vector<long long>({1}), 20),
      //CpTest("17.ex1", vector<long long>({}), vector)
      });


  auto s = find_if ( exemples.begin(), exemples.end(), [&cp] (CpTest i) {
      return launch_test(cp, i);
  }); 

  
  if ( s == exemples.end() ) {
    cout << "IntCode tests ok" << endl;
  } else { 
    cout << "Error IntCode tests file:" << s->fname.c_str()+36 << endl << endl;

    cerr << "replay fault" << endl;
    cp.verbose = true;
    s->f = s->f;
    launch_test(cp, *s);
   
  }
}

#endif
