#include <vector>
#include <algorithm>
#include <numeric>
#include <future>



#define N 100     // maximum of first parameter 
#define M 100     // maximum of second parameter

using namespace std;



template<>
void usage<1>(){
  cout << "A programme for the IntCode compiler contains a numerical function taking two integers as input." << endl;
  cout << "The maximum of the function on intervalle [0,100]x[0,100] must be found." << endl;
  cout << "Crible is parallelised." << endl;
}


typedef crible_elt pair<int,int>;


constexpr auto t_crible = [] (int u, const IntCode_1& cp) -> crible_elt { 
  vector<int> c(M); 

  generate( c.begin(), c.end(), [&cp, &u]() {
    IntCode_1 cpp = cp;
    cpp.set(1,u);
    cpp.set(2,v);
    return cpp.start();
  });

  auto elt = max_element(c.begin(), c.end());

  return make_pair(*elt, elt-c.begin());
};


//TODO : override on std namespace
constexpr auto _less<future<crible_elt>> =  [] (future<int> & f, future<int> & g) -> bool { return f.get() < g.get(); };


constexpr auto coord = [] ()

template<>
const void day_2<1>( const IntCode_1& cp ){


  vector<future<crible_elt>> a(N);

  int i=0;
  generate( a.begin(), a.end(), [&] () -> future<crible_elt> { 
      return t_crible(i++,cp); 
      });

  cout << "Solution is:";
  cout << coord( max_element( a.begin(), a.end(), _less<future<crible_elt>> ));
}


