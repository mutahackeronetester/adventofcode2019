#include <iostream>
#include <fstream>
#include "IntCode_1.h"


#ifndef DAY_2
#define DAY_2



template<int SILV_OR_GOLD>
void usage(){
  std::cerr << "The " << (SILV_OR_GOLD ? "gold" : "silver") << " problem is not described." << std::endl;
};



template<int SILV_OR_GOLD>
const void day_2( const IntCode_1& cp ){
  std::cerr << "The " << (SILV_OR_GOLD ? "gold" : "silver") << " solution is not ready." << std::endl;
};



#endif /* DAY-2 */


#ifdef SILVER

#define SG 0 
#endif


#ifdef GOLD 

#include "day-2G.cpp"
#define SG 1
#endif

#ifndef SG  
#define SG 0  //no interactive mode, default is Silver 
#endif

#define INPUT "day-2.input"


using namespace std;


template<>
void usage<0>(){
  cout << "A programme for the IntCode compiler contains a numerical function taking two integers as input." << endl;
  cout << "It must run with the fixed input: 12, 2 to succeed silver grade." << endl;
};


template<>
const void day_2<0>( const IntCode_1& cp ){
  IntCode_1 cpp = cp;
  cpp.set(1,12);
  cpp.set(1,2);
  cout << "Silver solution is:" << cpp.start() << endl; 
};



int main(){

  IntCode_1 cp;
  ifstream ifs(INPUT); 
  ifs >> cp;

  usage<SG>();
  day_2<SG>(cp);
}

