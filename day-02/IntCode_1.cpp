#include "IntCode_1.h"


using namespace std;


static constexpr auto opcode_default = [] ( vector<int>::iterator& a ) -> void { 
  throw *a; 
};


static constexpr auto opcode_1 = [] ( vector<int>::iterator& a, IntCode_1& cp ) -> void {
  int r = *++a + *++a;
  cp.set(*++a, r);
};


static constexpr auto opcode_2 = [] ( vector<int>::iterator& a, IntCode_1& cp ) -> void {
  int r = *++a * *++a;
  cp.set(*++a, r);
};



int IntCode_1::start(){
  auto a = begin();
  while( true ){
    switch(*a){
      case 1:
        opcode_1(a, *this);
        break;
      case 2:
        opcode_2(a, *this);
        break;
      case 99:
        return get(0); 
      default:
        opcode_default(a);
    }
    a++;
  }
};


istream& operator>>(istream& is, IntCode_1& cp){
  cp.heap = vector<int>(0);
  int k;
  char p;
  do 
  { is >> k;
    cp.heap.push_back(k);
  }
  while ( is >> p );
  return is;
}


IntCode_1::IntCode_1(const IntCode_1& other ){
  heap = vector<int>(other.heap.size());
  copy(other.heap.begin(), other.heap.end(), heap.begin());
}





