#include <iostream>
#include <vector>

#ifndef INTCODE_1
#define INTCODE_1


class IntCode_1 {

  std::vector<int> heap; 

  friend std::istream& operator>>( std::istream& is, IntCode_1& cp);

  public:
  
  IntCode_1(){};
  IntCode_1(std::vector<int> h):heap(h){};
  IntCode_1( const IntCode_1& other);

  auto begin(){
    return heap.begin();
  }

  int get(int i){
    return heap[i];
  }

  void set(int i, int v){
    heap[i] = v;
  }

  int start();
};

#endif
