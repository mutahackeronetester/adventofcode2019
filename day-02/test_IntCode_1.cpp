#include <iostream>
#include <vector>
#include <cassert>
#include "IntCode_1_proto.h"

using namespace std;


//TODO : manage access right
void unit_test_operation_default( IntCode cp ){

  cp.set(0,28);
  bool b = false;
  try 
  {
    cp.start();
  } 
  catch ( int )  
  {
    b = true;
  }
  assert(b);

  cerr << "UT opcode_default successful" << endl;
}


void unit_test_operation_99( IntCode cp ){
  cp.set(0,99);
  assert( cp.start() == 99 );

  cerr << "UT opcode_99 successful" << endl;
}

void unit_test_operation_1( IntCode cp ){
  cp.set(0,1);
  assert( cp.start() == cp.get(1) + cp.get(2) );

  cerr << "UT opcode_1 successful" << endl;
}

void unit_test_operation_2( IntCode cp ){
  cp.set(0,2);
  assert( cp.start() == cp.get(1) * cp.get(2) );
  
  cerr << "UT opcode_2 successful" << endl;
}


int main() {

  IntCode cp(vector<int>{1,2,3,0,99});

  unit_test_operation_default(cp);

  unit_test_operation_99(cp);

  unit_test_operation_1(cp);

  unit_test_operation_2(cp);

  cerr << "end unit test IntCode_1" << endl;

  return 0;
}
