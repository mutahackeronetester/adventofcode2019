#include <iostream>
#include <fstream>
#include <thread>
#include <map>
#include <iterator>
#include <algorithm>


#include "../miscellaneous/IntCode.h"
#include "Point.h"

#define COL 23
#define ROW 45


#ifdef SILVER
#define INTERACTIVE 1 
#define SAMPLE_FRAME 1
#define REFRESH_RATE 60

#else 
#define INTERACTIVE 0 
#define SAMPLE_FRAME 10
#define REFRESH_RATE 6

#endif


using namespace std;

typedef size_t row_n;
typedef size_t col_n;
typedef multimap<row_n,col_n> pixels;
typedef pair<row_n,col_n> px;

struct Pong {
  int fw, fh, s;
  Point ball;
  Point paddle;
  pixels walls; 
  pixels blocks; 
    
  /* initialise a fresh game */
  Pong(IntCode& cp):s(0){

    bool loaded = false;
    int x,y,k;

    while (!loaded){

      x = cp.out->get( 1000, loaded );
      if (!loaded){
        y = cp.out->get();
        k = cp.out->get();
        switch( k ){
          case 1:
            walls.insert(make_pair(y,x));
            break;
          case 2:
            blocks.insert(make_pair(y,x));
            break;
          case 3:
            paddle = Point(x,y);
            break;
          case 4:
            ball = Point(x,y);
        }
      }
    }

    fw = max_element(walls.begin(), walls.end(), []( px px1, px px2 ){ 
        return px1.second < px2.second; })->second;
    fh = max_element(walls.begin(), walls.end(), []( px px1, px px2 ){ 
        return px1.first < px2.first; })->first;
  };


  bool try_update_score( const Point& p, const IntCode& cp){
    const bool b = p == Point(-1, 0);
    if (b) {
      s = cp.out->get();
      cerr << "update score:" << s << endl; 
    }
    return b;
  }

  
  void break_block( const Point& p){

    const auto range = blocks.equal_range(p.r); 

    const auto fi = find_if( range.first, range.second, [&p] ( auto p2 ){
        return p2.second == (col_n) p.c;
        });

    if ( fi == range.second ){
      cerr << "no block to break" << endl;
      //throw  "no block to break";

    } else {  
      cerr << "break block at:" << p << endl;
      blocks.erase( fi );
    }
  }


  void update( const IntCode& cp){ 

    const Point p( cp.out->get(), cp.out->get());

    if ( !try_update_score(p, cp) ){

      const int k = cp.out->get();
      switch( k ){
        case 0:
          break_block(p);
          break;

        case 3:
          cerr << "update paddle at:" << p << endl;
          paddle = p;
          break;

        case 4:
          cerr << "update ball at:" << p << endl;
          ball = p;
          break;

        default:
          cerr << "unexpected update code" << endl;
          throw "unexpected update code";
      }
    }
  }

};


/* print frame */
ostream& operator<<( ostream& out, const Pong& p ){

  cout << endl << "         SCORE : " << p.s << endl << endl;
  vector<char> line(p.fw+1); 
  for ( int i = 0; i < p.fh; i++){
    fill( line.begin(), line.end(),' ');

    if( p.ball.r == i ){ line[p.ball.c] = 'o';} 
    if( p.paddle.r == i ){ line[p.paddle.c] = 'T';} 

    auto rwal = p.walls.equal_range( i );
    auto rblo = p.blocks.equal_range( i );

    
    for_each( rwal.first, rwal.second, [&line] ( pair<row_n,col_n> px ){
        line[px.second] = '|';
        });

    for_each( rblo.first, rblo.second, [&line] ( pair<row_n,col_n> px ){
        line[px.second] = 'V';
        });

    copy( line.begin(), line.end(), ostream_iterator<char>(cout));
    cout << endl;
  }

  return out;
}




template<bool INTER>
void play(const IntCode& cp, const Pong& P);


template<>
void play<0>(const IntCode& cp, const Pong& P) {
  cp.in->put( P.ball.c < P.paddle.c ? -1 : P.ball.c != P.paddle.c );
}


template<>
void play<1>(const IntCode& cp, const Pong& P) {
  string kk = "";
  cout << "move with k <   > l " << endl;
  do {
    cin >> kk;
  } while ( kk.size() == 0 );

  cp.in->put( kk == "k" ? -1 : kk == "l");
}



int main(){

  IntCode cp;
  ifstream ifs("day-13.input");
  ifs >> cp;

  thread t = thread( [&cp] { cp.compute(); });  // power up the arcade
  cp.heap[0] = 2;                               // insert coins
  Pong P(cp);                                   // start the game 

  cout << P;

  thread d = thread( [&cp, &P] {                // update the screen
      int i=0;
      while ( true) {  
        i++;

        P.update(cp);

        if ( !(i % SAMPLE_FRAME )){
          cout << P << endl; 
        }
      }
  });


  thread i = thread( [&cp, &P] {                // keyboard or bot
      while (true) {
        this_thread::sleep_for( chrono::milliseconds(REFRESH_RATE));
        play<INTERACTIVE>(cp, P);
      }});


  t.join();
  i.detach();
  this_thread::sleep_for(chrono::milliseconds(60));
  d.detach();
  cout << "congratulation !!" << endl;
}
