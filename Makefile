DAY=day-2
SG = GOLD
CFLAGS = -g -Wall -O0
CC = /usr/bin/g++ 
SHELL = /bin/bash



makefile: default
	@$(SHELL) -c " cd $(DAY) && [[ -e Makefile ]] && make"   

default: 
	@$(SHELL) -c "cd $(DAY) && [[ -e Makefile ]] || \
	$(CC) $(CFLAGS) -o $(DAY).out -D$(SG) *.cpp"


clean:
	rm *.out
