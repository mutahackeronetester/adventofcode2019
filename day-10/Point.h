#include <iostream>
#include <tuple>

#ifndef POINT 
#define POINT

#define PI 3.1415926535
#define NORTH Point(0,-1)
#define SOUTH Point(0,1)
#define WEST Point(1,0)
#define EAST Point(-1,0)


int pgcd( const int aa, const int bb);

struct Point {
  int c,r;

  Point(){};
  Point(int a, int b):c(a),r(b){}

  // minus without reducing.
  void operator-=( const Point& other);
  void operator+=( const Point& other);
  void turnRight();
  void turnLeft();
  bool operator==( const Point& other);

  std::tuple<float, float> radial();
  
  int norme();
};

// minus and reduce coord to pair of integer primary between them
Point operator-( const Point& first, const Point& other);
Point operator-( const Point& other);
Point operator+( const Point& first, const Point& other);

// order on L1 norma
bool operator<( const Point& first, const Point& other);

// L1 distance to center
bool closerTo( const Point& first, const Point& second, const Point& center);

 


// enable unordered_set
//ref https://en.wikipedia.org/wiki/Unordered_associative_containers_(C%2B%2B)#Custom_hash_functions
namespace std {
  template<>
    class hash<Point> {
      public:
        size_t operator()(const Point& p) const{
          return hash<int>()( p.r << 8 + p.c);
        }
    };
}


std::ostream& operator<<( std::ostream& out, const Point& p );


#endif
