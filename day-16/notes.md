

The algorithm name : FFT is deceptive and this efficient algorithm does not apply here. A quick look at wikipedia notice the shape of the discrete Fourier transformation:        


  S(k) = sum_{n=0}^{N-1} s(n) e^{-2*i*\Pi*n*k/N}

on the other hand our problematic is to solve the computation of :

  S(k) = sum_{n=0}^{N-1} s(n) w[n/k]

where w is the discrete mask [ 0, 1, 0, -1 ], equivalent to: 
 
  w[n/k] = Im( e^{i*\Pi*(n+1)/k} )


Unable to find an equivalence between these two calculus, I have instead performed a trivial computation  with a quadratic cost for the silver part of the problem. Furthermore, the gold part of the probleme has been left partially unprocessed.

A more complete response was given on this post : 
https://www.reddit.com/r/adventofcode/comments/ebxz7f/2019_day_16_part_2_visualization_and_hardmode  
