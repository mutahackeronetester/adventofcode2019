#include <iostream>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iterator>


#include "../day-5/IntCode.h"

#ifndef INTCODE_3
#define INTCODE_3


/**
 * Ad-oc compiler specified and tested step by step by problems : 
 * day-2 
 * day-5 
 *
 *
 *
 */
class IntCode_3 : public IntCode_2 {

  /* base used for relative adresses
   */
  int r_base = 0;

  template<6>
  void operation( std::vector<int>::iterator i);

  template<7>
  void operation( std::vector<int>::iterator i);


  // overriden
  /* unfolded read of argument, 
   * with absolute or relative address or with value.
   * */
  void one_arg( std::vector<T>::iterator i, int& b, long long* a);

};




#endif



