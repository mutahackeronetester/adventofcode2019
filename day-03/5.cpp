#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iterator>


using namespace std;



int main(){
  
  vector<vector<int>*> TH, TV; // TH holds Y coordinate of horizontal segments, 
                              // symetric for TV 
  vector<bool> BH;
  string s; 
  char* d;

  while(  getline(cin, s) ){

    vector<int> *h=new vector<int>({0}), *v=new vector<int>({0});
    d = strtok(const_cast<char*>( s.c_str() ), ",");
    BH.push_back( *d == 'R' || *d == 'L' );
    while (d != NULL){
      switch (*d){
        case 'R':
          v->push_back(  v->back() + atoi(++d) );
          break;
        case 'L':
          v->push_back(  v->back() - atoi(++d));
          break;
        case 'U':
          h->push_back( h->back() + atoi(++d));
          break;
        case 'D':
          h->push_back( h->back() - atoi(++d));
          break;
        default:
          cerr << "Wrong format." << *d << endl;
      } 
      d = strtok( NULL, ",");
    }

    cerr << "h:" << h->size() << endl;
    cerr << "v:" << v->size() << endl;
    // This test is not enough-> We trust data to be well formed.  
    if ( h->front() == 0 ){
      assert( 0<= h->size() - v->size() <=1  );
    } else {
      assert( 0<= v->size() - h->size() <=1  );
    }

    TH.push_back(h);
    TV.push_back(v);
  }


  cerr << "TH[0]:" << TH[0]->size() << endl;
  cerr << "TV[0]:" << TV[0]->size() << endl;
  copy( TV[0]->begin(), TV[0]->end(), ostream_iterator<int>(cerr, " "));

  vector<pair<int, int>> I;

  /*
   * test every combination of the TH.size() trajectories  
   */
  for ( int n = 0; n < TH.size(); n ++){
    for ( auto m = 0; m < TH.size(); m++){
      if ( n == m ){
        continue;
      }

      cerr << endl;
      cerr << endl;
      cerr << "n:" << n << " m:" << m << endl;
      cerr << "BH[n] :: " << BH[n] << endl;
      cerr << "TH[n] :: [ ";   
      copy( TH[n]->begin(), TH[n]->end(), ostream_iterator<int>(cerr, ", "));
      cerr << "] " << endl << "TV[n] :: [ "; 
      copy( TV[n]->begin(), TV[n]->end(), ostream_iterator<int>(cerr, ", "));
      cerr << "] " << endl;
      cerr << "BH[m] :: " << BH[m] << endl << "TH[m] :: [ ";
      copy( TH[m]->begin(), TH[m]->end(), ostream_iterator<int>(cerr, ", "));
      cerr << "] " << endl << "TV[m] :: [ "; 
      copy( TV[m]->begin(), TV[m]->end(), ostream_iterator<int>(cerr, ", "));
      cerr << "] " << endl;
      /*
       * inv: 
       *    for every segment horizontal of trajectory n,
       *    [iv, iv1], i are the coordinates of this segment.
       */
      vector<int>::iterator i = TH[n]->begin(), iv=TV[n]->begin(), iv1=iv;
      iv1++;
      if ( !BH[n] ){ i++; }
      do {
        /*
         * inv: 
         *    for every segment vertical of trajectory m,
         *    jv, [j, j1] are the coordinates of this segment.
         */     
        vector<int>::iterator j = TH[m]->begin(), j1=j, jv=TV[m]->begin();
        j1++;
        if ( BH[m] ){ jv++; }
        do {
          /*
           * test if segments are crossing.
           */
          if ( ( *j1 - *i ) * ( *j - *i) <= 0  ){
            if (( *iv1 - *jv ) * ( *iv - *jv ) <= 0 ){
              if ( *jv != 0 || *i != 0 ){
                I.push_back(make_pair(*jv, *i));
                cerr << "( x:" << *jv << ", y:" << *i << " ) ";
              }
            }
          }
        } while( j++, jv++, ++j1 != TH[m]->end() );
      } while ( i++, iv++, ++iv1 != TV[n]->end() );
      cerr << endl;
    }
  }


  cerr << "I:" << I.size() << endl;
  vector<int> a = vector<int>(I.size()) ;
  transform( I.begin(), I.end(), a.begin(), [] ( pair<int,int> p) {
      return abs( p.first ) + abs( p.second );
      });

  cerr << endl << "a[I] :";
  copy(a.begin(), a.end(), ostream_iterator<int>(cerr, " "));
  cerr << endl;
  
  cout << *min_element(a.begin(), a.end()) << endl;
}
