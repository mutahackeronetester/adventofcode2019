#include <iostream>

#ifdef SILVER
#define DISCR 0
#endif

#ifdef GOLD
#define DISCR 1
#endif

using namespace std;

int main(){
  

  int N,S=0;
  while( cin >> N, cin.ignore() ){
    N = N / 3 - 2;

    if ( DISCR ){
      while( S+=N, N= N/3 -2, N> 0 ){}

    } else {
      S+= N;
    }
  }
  
  cout << S << endl;
}
