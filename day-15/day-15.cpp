#include <iostream>
#include <list>
#include <fstream>
#include <thread>
#include <map>
#include <iterator>
#include <algorithm>


#include "../day-9/IntCode.h"
#include "Astar.h"

#define COL 60
#define ROW 20

using namespace std;

typedef size_t row_n;
typedef size_t col_n;
typedef multimap<row_n,col_n> pixels;


pair<row_n,col_n> to_pixel( const Point& p){
  return make_pair( p.r, p.c);
}

struct Area {

  int fh, fw, tl;
  Point droid;
  Point oSys;
  pixels walls; 
  pixels trace;

  Area(int a, int b):fh(a),fw(b),droid(b/2,a/2){};
  Area(const Area& other):fh(other.fh),fw(other.fw),droid(other.fw/2,other.fh/2){}


  void insert_trace( const Point& p ){
    auto rtra = trace.equal_range( p.r );
    if ( find_if( rtra.first, rtra.second, [&p] (auto tra) {
          return tra.second == p.c;
          }) == rtra.second ){
      trace.insert( to_pixel(p) );
    }
  }
  
};




ostream& operator<<( ostream& out, const Area& a ){

  out << endl;
  cout << "Trace : " << a.tl << endl;
  vector<char> line(a.fw+1); 
  for ( int i = 0; i < a.fh; i++){
    fill( line.begin(), line.end(),' ');

    auto rwal = a.walls.equal_range( i + ( a.droid.r - a.fh/2));
    auto rtra = a.trace.equal_range( i + ( a.droid.r - a.fh/2));
    
    for_each( rtra.first, rtra.second, [&line, &a] ( pair<row_n,col_n> px ){
        int d = px.second - ( a.droid.c - a.fw/2 );
        if ( 0 < d && d < a.fw+1  ) line[d] = '.';
        });

    for_each( rwal.first, rwal.second, [&line, &a] ( pair<row_n,col_n> px ){
        int d = px.second - ( a.droid.c - a.fw/2 );
        if ( 0 < d && d < a.fw+1 ) line[d] = '#';
        });

    if( a.fh/2 == i ) line[a.fw/2] = 'D'; 

    if( ! ( Point() == a.oSys )  && a.oSys.r == i + ( a.droid.r - a.fh/2) ) {
      int d = a.oSys.c - a.droid.c + a.fw/2;
      if ( d > 0 && d < a.fw+1 ) line[d] = 'X'; 
    }

    
    copy( line.begin(), line.end(), ostream_iterator<char>(out));
    out << endl;

  }

  return out;
}



int manuali(IntCode* cp, Point p){

    if( p == EAST ){
      cerr << "EAST" << endl;
      cp->in->put(3);
    }
    else if( p == WEST ){
      cp->in->put(4);
      cerr << "WEST" << endl;
    }
    else if( p == NORTH ){
      cp->in->put(1);
      cerr << "NORTH" << endl;
    }
    else if( p == SOUTH ){
      cp->in->put(2);
      cerr << "SOUTH" << endl;
    }

    return cp->out->get();
}

 /* 
struct Path {

  IntCode cp;
  Area* A;
  thread t;

  Node pos; //pos of the droid


  Path(IntCode cp, Area* A):cp(cp),A(A){
    t thread( [] { cp.compute(); });
  }

  Path(const Path& other):A(other.A),cp(other.cp){
    t = thread( [] { cp.compute();} );
    for ( Point p : other.path ){
      extend(p);
    }
  }


  Node bestPath( Point p ){
    Point obj = pos.coord + p;
    Node* bestPos = pos.origin;
    while( find( bestPos->neighbours.begin(), bestPos.neighbours.end(), 
          obj - bestPos->coord ) == bestPos->neightbours.end() )

  }

  int extend( Point p ){

    if( p == EAST ){
      cp.in.put(3);
    }
    else if( p == WEST ){
      cp.in.put(4);
    }
    else if( p == NORTH ){
      cp.in.put(1);
    }
    else if( p == SOUTH ){
      cp.in.put(2);
    }

    switch( cp.out.get()){
      case 0:
        A->walls.insert( to_pixel( p + pos.coord ));
        pos.neighbours.erase(p);
      case 1:
        A->droid+=p;
        pos = pos + p;
    }

    return cp.out.get();
  } 
};


bool operator<( const Path& p1, const Path& p2 ){
  return p1.path.size() < p2.path.size();
}

*/

void asi( IntCode* cp, Area* A, Astar* As){
  /* a start algorithm
   * chose a path to extends whose weight is minimum.
   *
   * */

  Node pos;

  while( As->extensible.size() > 0 ){

    Node& next = As->chooseOne();

    vector<Point> cmds = pos.pathTo( next );

    for_each( cmds.begin(), --cmds.end(), [&cp] (Point p) {
        if ( ! manuali(cp, p) ) { 
          throw "cannot reach ancestor";
        }
      });

    pos = *( next.origin );
    A->droid = pos.coord;
    A->tl = pos.l; 
    
   // cout << *A;
   // this_thread::sleep_for( chrono::milliseconds(10) );

    Point cmd = cmds.back();
    int ret = manuali( cp, cmd ); 

    if ( ret == 2 ){
      A->oSys = next.coord;
      As->new_origin(pos);
      cout << *A;
    }
    else if ( ret == 1 ){
      A->insert_trace( pos.coord );
      A->droid += cmd;
      A->tl += 1;
      pos = next;
      As->extend( &next, true );
    }
    else if ( ret == 0 ){
      A->walls.insert( to_pixel( next.coord ));
      As->extend( &next, false );
    }

   // this_thread::sleep_for( chrono::milliseconds(30) );
    cout << *A;
  } 

}


int main(){

  ifstream ifs;
  IntCode cp;

  ifs.open("day-15.input", ifstream::in);
  ifs >> cp;

  Area A(COL,ROW);
  Astar As;

  cout << A;

  thread t ( [&cp] { cp.compute(); });


  thread i ( asi, &cp, &A, &As);

/*
  thread d = thread( [&A, &cmd, &cp] {
    bool finished = false;
    int x;
    while (!finished){

      x = cp.out->get();

      switch( x ){
      case 0:
        cmd += A.droid;
        A.walls.insert(make_pair(cmd.r, cmd.c));
        break;
      case 1:
        A.trace.insert(make_pair(A.droid.r, A.droid.c));
        A.droid += cmd;
        break;
      case 2:
        cmd += A.droid;
        A.oSys = cmd;
        //finished = true;
      }
      cerr << "o" << endl;
      cout << A;
    }
  });
  */

  i.join();
  cerr << "finished mapping" << endl;
  t.detach();


  list<Node*> head; 
  for_each( As.visited.begin(), As.visited.end(), [&head] (Node& n) {
      if ( n.extensions.size() == 0 ){
      head.push_back(&n);}});

  cout << "max path" << (*max_element(head.begin(), head.end(), [] ( Node* n1, Node* n2) { return n1->l < n2->l; }))->l << endl;
}
