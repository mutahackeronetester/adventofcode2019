#include <vector>
#include <list>
#include "../day-11/Point.h"

#ifndef ASTAR
#define ASTAR


struct Node {
  int l;
  Point coord;
  Node* origin;
  std::vector<Point> extensions;

  Node();
  Node( Node* origin, Point ext );
  Node( const Node& );

  /* path goes from this to node
   *
   */
  std::vector<Point> pathTo( const Node& dest );

  Point adjacent( const Node& n ) const;

  int calculate_length();
  
  bool operator==( const Node& n );

};


struct Astar {

  std::list<Node> extensible;
  std::list<Node> visited;

  Node& chooseOne();

  void extend( Node* n, bool success );

  Astar();

  void calculate_all_length_from( int a);

  void new_origin( Node& n);

};

#endif


