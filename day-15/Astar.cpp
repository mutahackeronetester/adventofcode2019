#include <algorithm>
#include "Astar.h"




using namespace std;



  
Node::Node():coord(Point(0,0)),origin(nullptr),l(0){}


Node::Node( Node* origin, Point p):origin(origin),l(origin->l+1),coord(origin->coord + p){}


Node::Node( const Node& n):origin(n.origin),l(n.l),coord(n.coord),extensions(n.extensions){};


vector<Point> Node::pathTo( const Node& dest ){
  // Comming from this, to dest

  const Node *ad = &dest, *at = this;

  vector<Node> ancestors_d, ancestors_t;
  vector<Point> path;

  vector<Node>::iterator aj;
  vector<Node>::reverse_iterator ai;

  ancestors_d.push_back( *ad );
  ancestors_t.push_back( *at );
  while ( 
      ai = find( ancestors_d.rbegin(), ancestors_d.rend(), *at ),
      aj = find( ancestors_t.begin(), ancestors_t.end(), *ad ),
      ai == ancestors_d.rend() && aj == ancestors_t.end() ){

    if ( ad->origin ){
      ad = ad->origin;
      ancestors_d.push_back( *ad );
    }
    if ( at->origin ){
      at = at->origin;
      ancestors_t.push_back( *at );
    }
  }

  // if *at is not found, then commun ancestor is *ad, 
  // in last position of ancestors_d  
  // 
  if ( ai == ancestors_d.rend() ) ai = ancestors_d.rbegin();    

  // symetrically 
  //
  if ( aj == ancestors_t.end() ) aj--;  

  int st = aj - ancestors_t.begin();
  ai++;
  int sd = ancestors_d.rend() - ai;
  path.resize( st + sd );

  transform( ancestors_t.begin(), aj, path.begin(), 
      [] ( Node& n ) {
      return n.adjacent(*n.origin);
      });

  transform( ai, ancestors_d.rend(), path.begin()+st, 
      [] ( Node& n ) {
      return n.origin->adjacent(n);
      });


  return path;
  }


Point Node::adjacent( const Node& n ) const {
  return n.coord - coord;
}

bool Node::operator==(const Node& n ){
  return coord == n.coord; 
} 



int Node::calculate_length(){
  l = origin == nullptr ? 1 : 1+ origin->calculate_length();
  return l; 
}

Astar::Astar():visited({Node()}){
  for ( Point p : {EAST,WEST,SOUTH,NORTH} ){
    extensible.push_back( Node( &visited.back(), p) );
  }
}

struct euristic {
  /*
  const vector<Node>* visited;

  euristic( const vector<Node>& visited ):visited(&visited){}


  bool detect_wall( Node& n){
    return find_if( visited->begin(), visited->end(), [&n] (Node& nv){
          Point d = n.adjacent( nv );
          return ( d.norme() == 1 && find( nv.extensions.begin(), nv.extensions.end(), -d ) == nv.extensions.end() );
          }) != visited->end();
  }
  */

  int operator()( const Node& n1, const Node& n2 ){
    return n1.l < n2.l;
  }
};

Node& Astar::chooseOne(){
  return *min_element( extensible.begin(), extensible.end(), euristic(  ) );
}


void Astar::extend( Node* n, bool success ){


  if ( !success ){
    auto fi = find ( extensible.begin(), extensible.end(), *n );
    cerr << "hit the wall : " << fi->coord;
    extensible.erase(fi);
    n = nullptr;

  } else { 


    Point por =  n->origin->adjacent(*n);
    n->origin->extensions.push_back(por);

    visited.push_back( *n );
    n = &visited.back();

    extensible.erase( find ( extensible.begin(), extensible.end(), *n ));
    

    for( Point p : {EAST, WEST, NORTH, SOUTH}){
      if ( p == -por ) continue;
      Node ne = Node( n, p); 
      extensible.push_back( ne );
    }
  }

}



void Astar::calculate_all_length_from(int a){

  vector<Node*> head;
  for( auto vi = visited.begin(); vi != visited.end(); vi++ ){ 
      if ( vi->extensions.size() == 0 ){ head.push_back(&*vi); } 
  }
 
  for( Node* v : head ){
    if ( v->l >= a ) { v->calculate_length(); }
  }

  for( Node& e : extensible){
    e.l = e.origin->l+1;
  }
}



void Astar::new_origin( Node& n){
  Node* nn = n.origin, *norigin, *nextension= &n; 
  n.l = 1;
  n.origin=nullptr;
   while ( nn != nullptr ){
    norigin = nn->origin;
    nn->origin = nextension;
    nn->l =  nextension->l+1;
    nextension = nn;
    nn = norigin;
  }
  calculate_all_length_from(0);
}

