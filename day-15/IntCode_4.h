#include <iostream>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iterator>


#include "../day-7/IntCode.h"

#ifndef INTCODE_4
#define INTCODE_4


/**
 * Ad-oc compiler specified and tested step by step by problems : 
 * day-2 
 * day-5 
 * day-7 
 * day-9 
 *
 * Used to obfuscate problems result. Like day-13 and day-15, that would 
 * otherwise be hackable by looking at problem input. 
 *
 */
/* Template because big integers must be supported for day-11 */ 
template<typename T>
class IntCode_4 : public IntCode_3 {


  // override @see IntCode_1
  /* "heap" hold the program computed, 
   * loaded from shell input or from file.
   * Cannot be const, because it is used to store variables as well.
   * */
  std::vector<T> heap;


  /* generic operation code.
   * returns default error.
   * every operation is a specialization of this template. 
   * */
  template<8>
  void operation( std::vector<T>::iterator i );

  template<9>
  void operation( std::vector<T>::iterator i );
 
};




#endif

