#include <vector>
#include <algorithm>
#include <numeric>
#include <memory>
#include <cassert>

#include "bigUnsigned.h"

using namespace std;


/** private **/

template<typename T>
auto bigUnsigned<T>::first() const {
  return find_if(part.begin(), part.end(), [] (T v) { return v > 0; });
}


/** accessors **/

template<typename T>
T bigUnsigned<T>::back() const {
    return part.size() == 0 ? 0 : part.back();
}



template<typename T>
T bigUnsigned<T>::val() const{

  auto re = first();
  assert ( part.end() - re < 3 );
  assert ( *re < half_p );

  return re == part.end() ? 0 : val( --part.end() );
}


template<typename T>
T bigUnsigned<T>::val( const auto& pos ) const{

  if ( pos == part.begin() ) return *pos;

  auto pos2 = pos;
  pos2--;

  return  (T)((*pos2 << half_b ) + *pos);
}


/** operations **/

template<typename T>
void bigUnsigned<T>::operator<< ( const int i ){
  for ( int j = 0; j < i; j++ ){
    part.push_back(0);
  }
}



bool bigUnsigned<T>::operator> ( const bigUnsigned<T>& other) const {

  auto ft = find_if( part.begin(), part.end(), [] (T v) { return v > 0;}); 
  auto fo = find_if( other.part.begin(), other.part.end(), [] (T v) { return v > 0;}); 


  if ( other.part.end() - fo > part.end() - ft ) return false;
  if ( other.part.end() - fo < part.end() - ft ) return true;

  auto op = [ &fo, &other ] (T v) { return  v != *(fo++); };
  auto diff = find_if( ft, part.end(), op ); 

  return  diff == part.end() ? false : *diff > *--fo;
}


template<typename T>
bool bigUnsigned<T>::operator==( const bigUnsigned<T>& other) const {
  return ! ( other > *this || *this > other ); 
}



template<typename T>
void bigUnsigned<T>::operator+= ( const bigUnsigned<T>& other ){

  if ( !other.part.size() ) return;

  reverse(part.begin(), part.end());
  if( part.size() < other.part.size() ) part.resize( other.part.size());
  T ret = 0;
  auto pos = part.begin();

  const auto op = [&ret, &pos] (T i1) {
    T sum = i1 + *pos + ret;
    ret = sum >> half_b;
    *pos++ = sum % half_p;
  };

  for_each( other.part.rbegin(), other.part.rend(), op);

  while ( pos != part.end() && ret ) op(0);
  if ( ret ) part.push_back(ret);

  while( ! *part.rbegin() ) part.pop_back();

  reverse(part.begin(), part.end());
}


template<typename T>
void bigUnsigned<T>::operator-=( const bigUnsigned<T>& other ){
  T ret = 0;
  auto pos = part.rbegin();

  const auto op = [&] (T v) {
      if ( v > 0) throw domain_error("negative value");
      return; 
    }

    if ( *pos < ret + v ){
      *pos = *pos + half_p - ret - v;
      ret = 1;
    } else {
      *pos = *pos - ret - v;
      ret = 0;
    }
    pos++;
  };

  for_each( other.part.rbegin(), other.part.rend(), op);

  while ( ret ) op(0);
}


template<typename T>
bigUnsigned<T> bigUnsigned<T>::operator+ ( const bigUnsigned<T>& other ) const{
  bigUnsigned<T> s = *this;
  s += other;
  return s;
}


template<typename T>
bigUnsigned<T> bigUnsigned<T>::operator* ( const bigUnsigned<T>& other ) const{

  const auto op = [&] ( bigUnsigned<T> s, T u ) {
    s << 1;
    bigUnsigned<T> pp = *this;
    transform( part.begin(), part.end(), pp.part.begin(), [&u] (T v) {
        return u * v;
        });
    s += pp;
    return s;
  };

  return accumulate(other.part.begin(), other.part.end(), (bigUnsigned<T>)0, op);
}



template<typename T>
bigUnsigned<T> bigUnsigned<T>::by( const bigUnsigned<T>& d, shared_ptr<bigUnsigned<T>> r ) const{

  auto pos_r = r->part.begin();
  bigUnsigned<T> ret;

  /*  handle r / d with r <= half_p * d
   * */
  const auto _op = [&r, &pos_r, &ret] ( T q, T d ) -> T {

    if ( pos_r == r->part.end() ) {
      *r += ret * q;
      return 0;
    }

    if ( !d ) {
      if ( q != -1 ) pos_r++;
      return q;
    }

    if( q == -1 ){
      q = r->val(pos_r) / d ;
      if ( !q ) q = r->val(++pos_r) / d ;
    }


    /**
     * slow division algorithm 
     * if q1 * d > r ;
     *  (q1 - q2) * ret >= q2 * dd - r 
     *  q2 = ( q1 * ret + r ) / ( ret + dd )
     *
     */
     bigUnsigned<T> dd = d;
    int shift_l = r->part.end() - pos_r - 1;
    dd << shift_l;
    auto pos_ret = ret.part.end() - (shift_l + 1);

    if ( dd * q > *r ) {

        T q2, ret2 = ret.val(pos_ret) + d; // ret is scalar since qd > ret
                                            // ret + dd is scalar since last digit of ret is null;
        q2 = q * ( ret.val(pos_ret) / ret2 ) + r->val(pos_r) / ret2;   
                                            // r->val() < q*d
                                            // r->val() / ret2 < q * ( d / ret + d )
                                            // add with first term : q2 < q 
        *r += ret * (q - q2);
        q = q2;
    }

    ret += dd;
    *r -= dd * q;
    pos_r++;
    return q;
  };


  const auto op = [&] ( bigUnsigned<T> q, T p ) -> bigUnsigned<T> {

    *r << 1;
    *r += p;
    q << 1;

    bigUnsigned<T> rr = *r;
    pos_r = r->part.begin();
    ret = 0;
    q += accumulate( d.part.begin(), d.part.end(), (T) -1, _op );

    //assert( ret == d );
    assert( d * q.back() + *r == rr );
    assert( d > *r );

    return q;
  };

  return accumulate( part.begin(), part.end(), (bigUnsigned<T>)0, op);
}

