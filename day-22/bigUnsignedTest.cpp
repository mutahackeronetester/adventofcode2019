#include <iostream>
#include <memory>
#include <cassert>

#include "bigUnsigned.h"

using namespace std;


void test_division(){

  bigUnsigned<long unsigned> num = 15, den = 3; 
  shared_ptr<bigUnsigned<long unsigned>> rest;

  num << 2; 
  den << 1;

  auto q = num.by( den, rest);

  assert( *rest == 0 );
  assert( q.val() == ( (unsigned long) 5 << 32 ) );
}

void unit_test_first() {

  bigUnsigned<unsigned long> x = 0L, y = 1L, z = 1L << 32;
  assert( x.first() == x.part.end() );
  assert( y.first() == --y.part.end() );
  assert( z.first() == z.part.end() - 2 );
}



int main(){

  test_division();
  
  cout << "test ok";
}
