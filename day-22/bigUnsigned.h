#include <vector>
#include <memory>

#ifndef BIGUNSIGNED
#define BIGUNSIGNED


template<typename T>
class bigUnsigned {
  static const int half_b = sizeof(T) * 4;
  static const T half_p = (T) 1 << ( sizeof(T) * 4);


  /* Store decomposition of considered integer in base half_p
   * elements are called "digits"
   * most significant digit is at front
   * */
  std::vector<T> part;

  /*returns iterator on first significant digit*/
  auto first() const;
  friend void unit_test_first();

  public:

  bigUnsigned(){};
  bigUnsigned(T val):part({ val >> half_b, val % half_p }){};

  /** accessors **/

  /* get least significant digit*/
  T back() const; 

  /* returns total value if it is within range of T 
   * throw domain_error otherwise */
  T val() const; 

  /* return value of the 2 digits at [--pos, pos]
   */
  T val( const auto& pos ) const;


  /** operations **/

  void operator<< ( const int i );

  bool operator> ( const bigUnsigned<T>& other) const;

  bool operator==( const bigUnsigned<T>& other) const;

  void operator+= ( const bigUnsigned<T>& other );

  void operator-=( const bigUnsigned<T>& other );

  bigUnsigned<T> operator+ ( const bigUnsigned<T>& other ) const;

  bigUnsigned<T> operator* ( const bigUnsigned<T>& other ) const;

  bigUnsigned<T> by( const bigUnsigned<T>& d, std::shared_ptr<bigUnsigned<T>> r ) const;

};


#include "bigUnsigned.cpp"

#endif
