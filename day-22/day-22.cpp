#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <memory>
#include <iostream>
#include <iterator>
#include <memory>
#include <cstdint>
#include <cassert>

#define GOLD

#ifndef SILVER 
#ifndef GOLD

#define DECK_SIZE 10
#define GOLD_CARD -7
#define ROUNDS  1
#define GOLD_CARD_POS 6

#endif 
#endif 


#ifdef SILVER

#define DECK_SIZE 10007
#define GOLD_CARD -2019
#define GOLD_CARD_POS 4684 
#define ROUNDS  1

#endif


#ifdef GOLD

#define DECK_SIZE 119'315'717'514'047 
#define GOLD_CARD -1
#define GOLD_CARD_POS 2020
#define ROUNDS  101'741'582'076'661

#endif



#define PRIMES_MIN_100 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541


using namespace std;

//typedef ( DECK_SIZE > 10**6 ? uint64_t : size_t) deckType; 
typedef uint64_t deckType; 

template<typename T>
T multiply(T a, T b, const T& mod){
  /* avoid overflow 
   * if a * 2 is not overflow 
   *
   *  a * b
   *  110 * 10101
   *
   *  res = 0
   *  b1 = 1 --> res += a
   *  b/=2  a*=2
   *
   * */ 
  if ( a >> ( sizeof(T)*8 - 1) ){
    throw domain_error("Overflow of a * 2");
  }

  T res = 0;
  while ( b ){
    if ( b % 2 ) res = ( res + a ) % mod;
    a = ( a * 2 ) % mod;
    b /= 2;
  }
  return res;
}


template<typename T>
T power(T a, int e, const T& mod){

  if ( !e )       return 1;
  if ( e == 1 )   return a;
  if ( e % 2 )    return multiply<T>(power<T>(a, e-1, mod), a, mod);
  else {
    T square = power<T>(a, e/2, mod);
    return multiply<T>(square, square, mod);
  }
}




template<typename T>
class bigUnsigned {
  static constexpr int half_b = sizeof(T) * 4;
  static constexpr T half_p = (T) 1 << ( sizeof(T) * 4);
  vector<T> part;

  public:

  bigUnsigned(){};
  bigUnsigned(T val):part({ val >> half_b, val % half_p }){};


  T back() const{
    return part.size() == 0 ? 0 : part.back();
  }


  void operator-=( const bigUnsigned<T>& other ){
    T ret = 0;
    auto pos = part.rbegin();

    const auto op = [&] (T v) {
      if ( pos == part.rend() ) {
        if ( v > 0) throw domain_error("negative value");
        return; 
      }

      if ( *pos < ret + v ){
        *pos = *pos + half_p - ret - v;
        ret = 1;
      } else {
        *pos = *pos - ret - v;
        ret = 0;
      }
      pos++;
    };

    for_each( other.part.rbegin(), other.part.rend(), op);

    while ( ret ) op(0);
  }

  void operator+= ( const bigUnsigned<T>& other ){

    if ( !other.part.size() ) return;

    reverse(part.begin(), part.end());
    if( part.size() < other.part.size() ) part.resize( other.part.size());
    T ret = 0;
    auto pos = part.begin();

    const auto op = [&ret, &pos] (T i1) {
      T sum = i1 + *pos + ret;
      ret = sum >> half_b;
      *pos++ = sum % half_p;
    };

    for_each( other.part.rbegin(), other.part.rend(), op);

    while ( pos != part.end() && ret ) op(0);
    if ( ret ) part.push_back(ret);

    while( ! *part.rbegin() ) part.pop_back();

    reverse(part.begin(), part.end());
  }


  void operator<< ( const int i ){
    for ( int j = 0; j < i; j++ ){
      part.push_back(0);
    }
  }


  bool operator> ( const bigUnsigned<T>& other) const {

    auto ft = find_if( part.begin(), part.end(), [] (T v) { return v > 0;}); 
    auto fo = find_if( other.part.begin(), other.part.end(), [] (T v) { return v > 0;}); 


    if ( other.part.end() - fo > part.end() - ft ) return false;
    if ( other.part.end() - fo < part.end() - ft ) return true;
    
    auto op = [ &fo, &other ] (T v) { return  v != *(fo++); };
    auto diff = find_if( ft, part.end(), op ); 

    return  diff == part.end() ? false : *diff > *--fo;
  }


  bool operator==( const bigUnsigned<T>& other) const {
    return ! ( other > *this || *this > other ); 
  }


  bigUnsigned<T> operator+ ( const bigUnsigned<T>& other ) const{
    bigUnsigned<T> s = *this;
    s += other;
    return s;
  }

  bigUnsigned<T> operator* ( const bigUnsigned<T>& other ) const{

    const auto op = [&] ( bigUnsigned<T> s, T u ) {
        s << 1;
        bigUnsigned<T> pp = *this;
        transform( part.begin(), part.end(), pp.part.begin(), [&u] (T v) {
          return u * v;
          });
        s += pp;
        return s;
      };

    return accumulate(other.part.begin(), other.part.end(), (bigUnsigned<T>)0, op);
  }



  bigUnsigned<T> by( const bigUnsigned<T>& d, shared_ptr<bigUnsigned<T>> r ) const{

    auto pos_r = r->part.begin();
    bigUnsigned<T> ret;

    /*  handle r / d with r <= half_p * d
     * */
    const auto _op = [&r, &pos_r, &ret] ( T q, T dd ) -> T {

      if ( pos_r == r->part.end() ) {
        *r += ret * q;
        return 0;
      }

      if ( !dd ) {
        if ( q != -1 ) pos_r++;
        return q;
      }

      if( q == -1 ){
        q = r->val(pos_r) / dd ;
        if ( !q ) q = r->val(++pos_r) / dd ;
      }

      /* invariant : ret*q + r */
      const int rp = r->part.end() - pos_r;
      while ( pos_r = r->part.end() - rp, 
              pos_r - r->first() < 2 && 
              r->val(pos_r) / dd < q ) {
        q--;
        *r += ret;
      }

      //T X = qq * r->val(pos_r) / dd; 




      bigUnsigned<T> up = dd; 
      up << ((r->part.end() - pos_r) - 1);
      ret += up;
      *r -= up * q;
      pos_r++;
      return q;
    };

    
    const auto op = [&] ( bigUnsigned<T> q, T p ) -> bigUnsigned<T> {

      *r << 1;
      *r += p;
      q << 1;

      bigUnsigned<T> rr = *r;
      pos_r = r->part.begin();
      ret = 0;
      q += accumulate( d.part.begin(), d.part.end(), (T) -1, _op );

      //assert( ret == d );
      assert( d * q.back() + *r == rr );
      assert( d > *r );

      return q;
    };

    return accumulate( part.begin(), part.end(), (bigUnsigned<T>)0, op);
  }

  auto first() const {
    return find_if(part.begin(), part.end(), [] (T v) { return v > 0; });
  }

  T val() const{

    auto re = first();
    assert ( part.end() - re < 3 );
    assert ( *re < half_p );

    return re == part.end() ? 0 : val( --part.end() );
  }


  T val( const auto& pos ) const{

    if ( pos == part.begin() ) return *pos;

    auto pos2 = pos;
    pos2--;

    return  (T)((*pos2 << half_b ) + *pos);
  }
};




template<typename T>
T divide_product( const T& a, const T& b, const T& c, const T& d1, const T& d2 ){
  /* 
   *  ^ ( a * b - c ) / (d1 * d2)
   *
   * */

    bigUnsigned<T> n = a, m = b, k, d=d1; 
    n = m * n;
    n -= c;
    m = 0;
    d = d * d2;

    auto r = make_shared<bigUnsigned<T>>( m );

    k = n.by( d, r );
    assert( r->val() == 0 );

    return k.val();
}





template<typename T>
T merge_inverse(const T& a, const T& b, const T& N, const T& pp1, const T& pp2){
  /*        a * N = 1 [pp1]
   *  and   b * N = 1 [pp2]
   *  =>    ( a*N - 1 )( b*N - 1 ) = 0 [pp1*pp2]  
   *  =>    ( a*b*N - a - b)N = -1 [pp1*pp2]  
   *        ---
   *  ^     a + b - a*b*N 
   * */

  assert( multiply<T>(a, N, pp1) == 1 );
  assert( !b || multiply<T>(b, N, pp2) == 1 );


  T pp3 = multiply<T>(pp1, pp2, N);

  T INV = multiply<T>( a, multiply<T>(b, N, pp3), pp3);
  INV = INV > a + b ? 
    pp3 + a + b - INV : 
    a + b - INV;

  /* correction if pp3 != pp1 * pp2
   *
   *  INV * N = 1 + k(pp1 * pp2) 
   *  pp3     = pp1 * pp2 + m * N
   * =>
   *  ( INV + k*m ) * N = 1 [pp3]
   * */
  
  const T m = divide_product<T>( pp1, pp2, pp3, N, 1 );
  if ( m ) {
    const T k = divide_product<T>( INV, N, 1, pp1, pp2 );
    INV += multiply<T>(k, m, pp3);
    INV %= pp3;
  }

  assert( multiply<T>( INV, N, pp3 ) == 1 );

  return INV;
}


static const vector<int> base_prime{PRIMES_MIN_100};


template<int BORNE_FRIABLE>
vector<int> decomp( int k ){
  const auto a = find_if( base_prime.begin(), base_prime.end(), [] (int p) { return p > BORNE_FRIABLE; });
  vector<int> dk(a - base_prime.begin());
  transform(base_prime.begin(), a, dk.begin(), [&k] (int p) { 
      int acc = 0;
      while ( !(k%p) ) {
        acc++; 
        k/=p;
      }
      return acc;
      });

  if ( k > 1 ) { 
    cerr << "try to inverse k:" << k << endl;
    throw domain_error("Integer not friable.");
  }

  return dk;
}


template<typename T>
T inverse( const int k, const T& N ) {
  /*  
   *  return INV such that N * INV % k = 1 
   *  with k friable 
   * */
  vector<int> dk = decomp<550>(k);

  transform(dk.begin(), dk.end(), base_prime.begin(), dk.begin(), [&k, &N] (int e, int p) {
      /* from little fermat:
       *    N ^ (p-1) = 1 [p]
       *  <=>
       *    ( N ^ (p-1) - 1 ) ^ e = 0 [p^e]
       *  <=>
       *    sum{ k <: 0..e-1 | a(e) } * N^(p-1) + (-1)^e 
       *                          = 0 [p^e]
       *  where:
       *    a(e) = (-1)^(e-k-1) * ( N^(p-1) - 1) ^ k   
       *
       *  lets call a_s the sum of geometric serie ae:
       *    reason:         - ( N^(p-1) - 1 ) 
       *    first element:  1
       *    size:           e 
       *  ----
       *  if e is even
       *    - a_s * N^(p-1) + 1 = 0 [k]
       *  if e is odd
       *      a_s * N^(p-1) - 1 = 0 [k]
       *  ----
       *    ^ a_s + N^(p-2) 
       *
       * */

      if ( ! e ) return 0;

      const int m = (int) power<T>(N, p-2, k);

      if ( e == 1 ) return m;

      int r = (int) multiply<T>(m, N, k);
      r = r ? r - 1 : k - 1;
      vector<int> a(e);
      a[0] = 1;

      const auto op = [ &k, &r ] (int ae) { 
        return k - ((ae * r)%k); 
      };

      transform(a.begin(), --a.end(), ++a.begin(), op);

      const int s = accumulate( a.begin(), a.end(), 0) % k; 
      
      return s * m % k;
      });


  const auto op = [&k, &N]( int s, int a ) { 
      // k is friable, we assume no overflow;
      return ( s + a - s * multiply<T>( a, N, k) ) % k; 
      };

  const int INV = accumulate(dk.begin(), dk.end(), 1, op);

  assert( INV*( N % k ) % k == 1 );

  return INV;
}


template<typename T>
class Deck {
  /* apply shuffle and retrieve cards */

  T N;
  T offset, inc, N_inv;         // offset keep track of the cuts
                                // inc keep track of deals with new stacks & deal with increments 

public:

  Deck():N(DECK_SIZE),inc(1),offset(0),N_inv(0){}
  /*
  void operator=(Deck<T>& other){ 
    offset = other.offset;
    inc = other.inc; 
    N_inv = other.N_inv
  };
  */

  T search( T card ) const {
    return ( multiply<T>(card, inc, N) + offset ) % N; 
  }

  T get( T pos ) const {
    pos = pos < offset ? N - offset + pos : pos - offset; 

    /*  look for:
     *           X * inc = pos [N]
     *  having:
     *         N_inv * N = 1 [inc]
     *  ----
     *         N_inv * N = 1 + k * inc 
     *  <=>
     *    -k * pos * inc = pos [N]
     *  with:
     *                 k = (N_inv*N - 1) / inc
     *  ----
     *                   ^ -k * pos [N]
     */

    const T k = divide_product<T>( N_inv, N, 1, inc, 1 );
    const T s = multiply<T>( pos, N - k, N);
    assert( multiply<T>( s, inc, N) == pos );
    return s;
  }

  void newstack(){
    offset = N-offset-1; // offset de 0 devient N-1
    inc = N-inc;
    /* 
     *          N_inv*N = 1 + k*(N-inc)
     * <=>
     *    (k-N_inv) * N = -1 + k*inc 
     * with: 
     *                k = ( N_inv*N - 1 ) / ( N-inc )
     * ------
     *                  ^ N_inv - k [inc]
     *
     * */

    const T k = divide_product<T>( N_inv, N, 1, N-inc, 1 );
    N_inv += inc - ( k%inc );
    N_inv %= inc;
    assert( multiply<T>( N_inv, N, inc) == 1 );
  }

  void cut(T n){
    offset += ( n > 0 ? N-n : -n );
    offset %= N;
  }

  void incremental(int n){
    offset = ( n * offset ) % N;
    N_inv = merge_inverse<T>( inverse(n, N), N_inv, N, n, inc ); 
    inc = ( n*inc ) % N;
    assert( multiply<T>( N_inv, N, inc) == 1 );
  }


  void repeat( Deck& other ){
    offset = (offset + other.offset) % N;
    N_inv = merge_inverse<T>( other.N_inv, N_inv, N, other.inc, inc ); 
    inc = multiply<T>(other.inc, inc, N);
    assert( multiply<T>( N_inv, N, inc) == 1 );
  }

  template<typename D>
  friend ostream& operator<<( ostream& out, Deck<D>& d); 
};


template<typename T>
ostream& operator<<(ostream& out, Deck<T>& d){
  copy( d.deck.begin(), d.deck.end(), ostream_iterator<int>(out, " "));
  out << endl;
  return out;
}



class Action {
  /* parse folding process */
  int a, b;
public:

  Action(string line){
   if( line[0] == 'c' ){          // "cut X"
      a = 0;
      b = atoi(line.substr(4, line.size()-4).c_str());

    } else if ( line[5] == 'i'){  // "deal into new stack"
      a = 1;

    } else {                      // "deal with increment X"
      a = 2;
      b = atoi(  line.substr(20, line.size() - 20).c_str());
    }
  }

  void apply( Deck<deckType>& deck) const{
    switch (a){
      case 0:   deck.cut(b);          break;
      case 1:   deck.newstack();      break;
      case 2:   deck.incremental(b);  break;
    }
  }
};


int main(){

  string line, sub;
  vector<Action> process;
  Deck<deckType> deck, half_deck;
  deckType round = ROUNDS;


  while( getline( cin, line)){
    process.push_back(Action(line));
  };

  for( Action a : process ){
    a.apply(deck);
  }


  while ( round ){

    if( round % 2 ){
      half_deck = deck;
      deck.repeat( deck );
      deck.repeat( half_deck );

    } else {
      deck.repeat( deck );
    }
  
    round = round >> 1;
    cerr << "round:" << round << endl;
  }


  if ( GOLD_CARD > 0 ){
    cout << deck.search( GOLD_CARD ) << endl;
  } else if ( GOLD_CARD_POS > 0 ){
    cout << deck.get( GOLD_CARD_POS ) << endl; 
  }
}
