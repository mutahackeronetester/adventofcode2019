
Published by Eric Wastl, Advent of Code is an Advent calendar that publishedtwo programmind puzzles a day, addressing a variety of skill sets, that can be used for training or speed contest. 

Take a look at official website : https://adventofcode.com/

completion : 33 / 50

********************************************

Puzzle Index 

day-01 : Compute the amount of fuel needed to launch space module, recursive since fuel has mass. 

day-02 : First implementation of the IntCode compiler with operations : terminate, + and *. Brut force the maximum value of the input program. Calcul parallelization.

day-03 : Find the closest intersections of wires in a 2D graph.

day-04 : Count the number of integers with crescent figures and one pair of identical adjacent figures.  

day-05 : Refinement of the IntCode compiler, with operations print, scan, goto_if_equal, goto_if_null.    

day-06 : Find distance between YOU and SAN in the orbit map of the solar system, which has a tree like structure.  

day-07 : New refinement of the IntCode compiler. Find the right configuration of a chain of 5 amplificators in open or in closed loop. Multithread synchronization.

day-08 : Compose an image out of several frames.

day-09 : Final refinement of IntCode compiler.

day-10 : Find which position has the most objects in sight, on a 2D grid.  

day-11 : Follow a smart painting robot until it finishes the picture. 

day-12 : Simulate a 4-body system, with gravity interaction, and estimate the (very long) time that it takes to return to its initial position.  

day-13 : Win the Pong game. 

day-14 : Reduce a list of chemical reaction to get the optimum amount of FUEL crafted. 

day-15 : Explore 2D map with a drone, and find the shortest path with A* algorithm. 

day-16 : Compute a long incomming signal with a simili-FFT transformation 

-----

-----

-----

-----

-----

day-22 : Shuffle a card deck with ~ 10^11 cards, then find the card in position 2020. Z/nZ calculation, with n the number of cards. Find the inverse inv so that inv * k = 2020 [n] with k depends of the shuffle process. 

-----

-----

-----







